# 人口普查可视化

#### 介绍
1953~2021年七次全国人口普查以及各省人口数量变化情况


公众号：Python研究者
作者：李运辰

![输入图片说明](https://images.gitee.com/uploads/images/2021/0511/215049_bb25eb57_1315661.gif "1953~2021年不同省份总人口.gif")


![输入图片说明](https://images.gitee.com/uploads/images/2021/0511/215101_8b247862_1315661.gif "第七次人口普查不同省份总人口.gif")


![输入图片说明](https://images.gitee.com/uploads/images/2021/0511/215111_b5558462_1315661.png "微信图片_20210511213029.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0511/215129_007d110b_1315661.jpeg "底部双码.jpg")